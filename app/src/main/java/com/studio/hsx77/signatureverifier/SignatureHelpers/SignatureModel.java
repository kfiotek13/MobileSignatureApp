package com.studio.hsx77.signatureverifier.SignatureHelpers;

import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

public class SignatureModel {

    //region Private Fields

    private static final int numberOfSignatureSamples = 5;

    private boolean isModelCreationCorrect;

    private double maxTimeOfSignatureCreation, minTimeOfSignatureCreation;

    private static double[]
            xResampledCoordinatesSample1, yResampledCoordinatesSample1, timeResampledPointsSample1, touchingResampledSample1,
            xResampledCoordinatesSample2, yResampledCoordinatesSample2, timeResampledPointsSample2, touchingResampledSample2,
            xResampledCoordinatesSample3, yResampledCoordinatesSample3, timeResampledPointsSample3, touchingResampledSample3,
            xResampledCoordinatesSample4, yResampledCoordinatesSample4, timeResampledPointsSample4, touchingResampledSample4,
            xResampledCoordinatesSample5, yResampledCoordinatesSample5, timeResampledPointsSample5, touchingResampledSample5;

    private double[] modelSignatureX, modelSignatureY, modelSpeedOfSignature, modelSignatureTouchings;

    //endregion

    //region Constructors

    /**
     * Create the model signature for the user
     */
    public SignatureModel() {
        isModelCreationCorrect=true;
        Thread modelSignatureThread = new Thread("Model Signature Thread") {
            public void run() {
                BeginProcessOfCreatingTheModelSignature();
            }
        };
        modelSignatureThread.start();
        try {
            modelSignatureThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //endregion

    //region Private Methods

    private void BeginProcessOfCreatingTheModelSignature() {
        try {
            ResampleSignatures();
            CreateModelSignature();
            SetMaxAndMinTimeOfWritingSignature();
        } catch (Exception e) {
            isModelCreationCorrect=false;
            e.printStackTrace();
        }
    }

    /**
     * Resample all signature samples to obtain samples with the same size.
     */
    private void ResampleSignatures() throws Exception {
        for (int i = 1; i <= numberOfSignatureSamples; i++) {

            PolynomialSplineFunction xSignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(SignatureCollector.GetXCoordinates(i), 1000));
            PolynomialSplineFunction ySignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(SignatureCollector.GetYCoordinates(i), 1000));
            PolynomialSplineFunction speedSignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(SignatureCollector.GetTimePoints(i), 1000));
            PolynomialSplineFunction touchingToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(SignatureCollector.GetTouchings(i), 1));

            SetXResampledCoordinates(i, SignalModelling.ResampleData(SignatureCollector.GetMaxSignatureLength(), SignatureCollector.GetSignatureNumberOfPoints(i - 1), xSignalToResample));
            SetYResampledCoordinates(i, SignalModelling.ResampleData(SignatureCollector.GetMaxSignatureLength(), SignatureCollector.GetSignatureNumberOfPoints(i - 1), ySignalToResample));
            SetTimeResampledPoints(i, SignalModelling.ResampleData(SignatureCollector.GetMaxSignatureLength(), SignatureCollector.GetSignatureNumberOfPoints(i - 1), speedSignalToResample));
            SetResampledTouchings(i, SignalModelling.ResampleData(SignatureCollector.GetMaxSignatureLength(), SignatureCollector.GetSignatureNumberOfPoints(i - 1), touchingToResample));
        }
    }

    /**
     * Create the model signature made of all resampled signature samples.
     */
    private void CreateModelSignature() throws Exception {

        int maxSignatureLength = SignatureCollector.GetMaxSignatureLength();

        double[] tempSameXCoordinatesTable = new double[numberOfSignatureSamples];
        double[] tempSameYCoordinatesTable = new double[numberOfSignatureSamples];
        double[] tempSameSpeedCoordinatesTable = new double[numberOfSignatureSamples];
        double[] tempSameTouchingsTable = new double[numberOfSignatureSamples];

        modelSignatureX = new double[maxSignatureLength];
        modelSignatureY = new double[maxSignatureLength];
        modelSpeedOfSignature = new double[maxSignatureLength];
        modelSignatureTouchings = new double[maxSignatureLength];

        for (int j = 0; j < maxSignatureLength; j++) { // For each coordinate of signature

            for (int i = 1; i <= numberOfSignatureSamples; i++) { // For each sample of signature of the chosen user

                double[] resampledXSignature = GetXResampledCoordinates(i); // Resampled samples of a next sample of signature
                double[] resampledYSignature = GetYResampledCoordinates(i);
                double[] resampledSpeedOfSignature = GetResampledTimePoints(i);
                double[] resampledTouchingOfSignature = GetResampledTouchings(i);

                tempSameXCoordinatesTable[i - 1] = resampledXSignature[j]; //Fill temp array by ith sample of each signature
                tempSameYCoordinatesTable[i - 1] = resampledYSignature[j];
                tempSameSpeedCoordinatesTable[i - 1] = resampledSpeedOfSignature[j];
                tempSameTouchingsTable[i - 1] = resampledTouchingOfSignature[j];
            }
            modelSignatureX[j] = GetAverageValue(tempSameXCoordinatesTable);
            modelSignatureY[j] = GetAverageValue(tempSameYCoordinatesTable);
            modelSpeedOfSignature[j] = GetAverageValue(tempSameSpeedCoordinatesTable);
            modelSignatureTouchings[j] = GetAverageValue(tempSameTouchingsTable);
        }
    }

    private double GetAverageValue(double[] coordinatesTable) {
        return GetSumOfValues(coordinatesTable) / numberOfSignatureSamples;
    }

    private double GetSumOfValues(double[] tableToSumUp) {
        double sum = 0.0d;
        for (double valueToSum: tableToSumUp) {
            sum += valueToSum;
        }
        return sum;
    }

    private int[] ExtractModelOfTouchingSignature(double[] arrayToFindLocalMinimum) {

        int[] newArray = new int[arrayToFindLocalMinimum.length];

        if (arrayToFindLocalMinimum[0] != 0.0d) {
            arrayToFindLocalMinimum[0] = 0.0d;
            newArray[0] = 0;
        }
        for (int i = 1; i < newArray.length; i++) {
            newArray[i] = 1;
        }

        for (int i = 1; i < arrayToFindLocalMinimum.length; i++) {
            if (arrayToFindLocalMinimum[i] < 1.0d) {
                if (arrayToFindLocalMinimum[i] < arrayToFindLocalMinimum[i - 1]) {
                    if (arrayToFindLocalMinimum.length < i + 1) {
                        if (arrayToFindLocalMinimum[i] < arrayToFindLocalMinimum[i + 1]) {
                            newArray[i] = 0;
                        }
                    }
                }
            }
        }
        return newArray;
    }

    //endregion

    //region Properties

    //region Public Properties

    public double[] GetModelSignatureX() {
        return modelSignatureX;
    }

    public double[] GetModelSignatureY() {
        return modelSignatureY;
    }

    public double[] GetModelSpeedOfSignature() {
        return modelSpeedOfSignature;
    }

    public int[] GetModelSignatureTouchings() {
        return ExtractModelOfTouchingSignature(modelSignatureTouchings);
    }

    public double GetMaxTimeOfSignatureCreation() {
        return maxTimeOfSignatureCreation;
    }

    public double GetMinTimeOfSignatureCreation() {
        return minTimeOfSignatureCreation;
    }

    public boolean GetInformationIfModelCreationWasCorrect(){return isModelCreationCorrect;}
    //endregion

    //region Private Properties

    private static double[] GetXResampledCoordinates(int numberOfSignatureSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                return xResampledCoordinatesSample1;
            case 2:
                return xResampledCoordinatesSample2;
            case 3:
                return xResampledCoordinatesSample3;
            case 4:
                return xResampledCoordinatesSample4;
            case 5:
                return xResampledCoordinatesSample5;
            default:
                throw new Exception("Wrong X resampled coordinate number");
        }
    }

    private static double[] GetYResampledCoordinates(int numberOfSignatureSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                return yResampledCoordinatesSample1;
            case 2:
                return yResampledCoordinatesSample2;
            case 3:
                return yResampledCoordinatesSample3;
            case 4:
                return yResampledCoordinatesSample4;
            case 5:
                return yResampledCoordinatesSample5;
            default:
                throw new Exception("Wrong Y resampled coordinate number");
        }
    }

    private static double[] GetResampledTimePoints(int numberOfSignatureSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                return timeResampledPointsSample1;
            case 2:
                return timeResampledPointsSample2;
            case 3:
                return timeResampledPointsSample3;
            case 4:
                return timeResampledPointsSample4;
            case 5:
                return timeResampledPointsSample5;
            default:
                throw new Exception("Wrong time resampled points number");
        }
    }

    private static double[] GetResampledTouchings(int numberOfSignatureSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                return touchingResampledSample1;
            case 2:
                return touchingResampledSample2;
            case 3:
                return touchingResampledSample3;
            case 4:
                return touchingResampledSample4;
            case 5:
                return touchingResampledSample5;
            default:
                throw new Exception("Wrong touching resampled number");
        }
    }


    private void SetXResampledCoordinates(int numberOfSignatureSample, double[] xCoordinatesSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                xResampledCoordinatesSample1 = xCoordinatesSample;
                break;
            case 2:
                xResampledCoordinatesSample2 = xCoordinatesSample;
                break;
            case 3:
                xResampledCoordinatesSample3 = xCoordinatesSample;
                break;
            case 4:
                xResampledCoordinatesSample4 = xCoordinatesSample;
                break;
            case 5:
                xResampledCoordinatesSample5 = xCoordinatesSample;
                break;
            default:
                throw new Exception("Wrong X resampled coordinate number");
        }
    }

    private void SetYResampledCoordinates(int numberOfSignatureSample, double[] yCoordinatesSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                yResampledCoordinatesSample1 = yCoordinatesSample;
                break;
            case 2:
                yResampledCoordinatesSample2 = yCoordinatesSample;
                break;
            case 3:
                yResampledCoordinatesSample3 = yCoordinatesSample;
                break;
            case 4:
                yResampledCoordinatesSample4 = yCoordinatesSample;
                break;
            case 5:
                yResampledCoordinatesSample5 = yCoordinatesSample;
                break;
            default:
                throw new Exception("Wrong Y resampled coordinate number");
        }
    }

    private void SetTimeResampledPoints(int numberOfSignatureSample, double[] timePointsSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                timeResampledPointsSample1 = timePointsSample;
                break;
            case 2:
                timeResampledPointsSample2 = timePointsSample;
                break;
            case 3:
                timeResampledPointsSample3 = timePointsSample;
                break;
            case 4:
                timeResampledPointsSample4 = timePointsSample;
                break;
            case 5:
                timeResampledPointsSample5 = timePointsSample;
                break;
            default:
                throw new Exception("Wrong resampled time point number");
        }
    }

    private void SetResampledTouchings(int numberOfSignatureSample, double[] touchingSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                touchingResampledSample1 = touchingSample;
                break;
            case 2:
                touchingResampledSample2 = touchingSample;
                break;
            case 3:
                touchingResampledSample3 = touchingSample;
                break;
            case 4:
                touchingResampledSample4 = touchingSample;
                break;
            case 5:
                touchingResampledSample5 = touchingSample;
                break;
            default:
                throw new Exception("Wrong resampled touching number");
        }
    }

    private void SetMaxAndMinTimeOfWritingSignature() throws Exception {
        maxTimeOfSignatureCreation = minTimeOfSignatureCreation =  //min/max initialization
                GetSumOfValues(SignalModelling.NormalizeData(SignatureCollector.GetTimePoints(1), 1000));

        for (int i = 2; i <= numberOfSignatureSamples; i++) {

            double[] signatureSpeed = SignalModelling.NormalizeData(SignatureCollector.GetTimePoints(i), 1000);
            double sumOfSignatureSpeeds = GetSumOfValues(signatureSpeed);

            if (sumOfSignatureSpeeds > maxTimeOfSignatureCreation)
                maxTimeOfSignatureCreation = sumOfSignatureSpeeds;
            if (sumOfSignatureSpeeds < minTimeOfSignatureCreation)
                minTimeOfSignatureCreation = sumOfSignatureSpeeds;
        }
    }
    //endregion

//endregion
}
