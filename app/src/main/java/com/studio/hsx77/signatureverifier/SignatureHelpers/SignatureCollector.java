package com.studio.hsx77.signatureverifier.SignatureHelpers;

import java.util.ArrayList;

public class SignatureCollector {

    //region Private Fields

    private static int numberOfSignatureSamples = 0;
    private static int maxSignatureLength = 0;

    private static ArrayList<Integer> signatureNumberOfPointsList = new ArrayList<>();

    private static double[]
            xCoordinatesSample1, yCoordinatesSample1, timePointsSample1,touchingSample1,
            xCoordinatesSample2, yCoordinatesSample2, timePointsSample2,touchingSample2,
            xCoordinatesSample3, yCoordinatesSample3, timePointsSample3,touchingSample3,
            xCoordinatesSample4, yCoordinatesSample4, timePointsSample4,touchingSample4,
            xCoordinatesSample5, yCoordinatesSample5, timePointsSample5,touchingSample5;

    //endregion

    //region Constructors

    public SignatureCollector(ArrayList<Float> xCoordinatesList, ArrayList<Float> yCoordinatesList, ArrayList<Float> timePointsList, ArrayList<Double> touchingsList) throws Exception {
        numberOfSignatureSamples++;

        if (maxSignatureLength < xCoordinatesList.size())
            maxSignatureLength = xCoordinatesList.size();

        SetSignatureNumberOfPoints(xCoordinatesList.size());

        SetXCoordinates(numberOfSignatureSamples, FloatToDoubleConverter(xCoordinatesList));
        SetYCoordinates(numberOfSignatureSamples, FloatToDoubleConverter(yCoordinatesList));
        SetTimePoints(numberOfSignatureSamples, FloatToDoubleConverter(timePointsList));
        SetTouchings(numberOfSignatureSamples, ListToArrayConverter(touchingsList));
    }

    //endregion

    //region Private Methods

    private double[] ListToArrayConverter(ArrayList<Double> listToConvert) {
        double[] convertedTable = new double[listToConvert.size()];

        for (int i = 0; i < listToConvert.size(); i++) {
            convertedTable[i] = listToConvert.get(i);
        }
        return convertedTable;
    }

    private double[] FloatToDoubleConverter(ArrayList<Float> listToConvert) {
        double[] convertedTable = new double[listToConvert.size()];

        for (int i = 0; i < listToConvert.size(); i++) {
            convertedTable[i] = (double) listToConvert.get(i);
        }
        return convertedTable;
    }

    //endregion

    //region Properties

    public static int GetNumberOfSignatureSamples() {
        return numberOfSignatureSamples;
    }

    public static int GetMaxSignatureLength() {
        return maxSignatureLength;
    }

    public static int GetSignatureNumberOfPoints(int numberOfSignatureSamples) {
        return signatureNumberOfPointsList.get(numberOfSignatureSamples);
    }

    public static double[] GetXCoordinates(int numberOfSignatureSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                return xCoordinatesSample1;
            case 2:
                return xCoordinatesSample2;
            case 3:
                return xCoordinatesSample3;
            case 4:
                return xCoordinatesSample4;
            case 5:
                return xCoordinatesSample5;
            default:
                throw new Exception("Wrong X coordinate number");
        }
    }

    public static double[] GetYCoordinates(int numberOfSignatureSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                return yCoordinatesSample1;
            case 2:
                return yCoordinatesSample2;
            case 3:
                return yCoordinatesSample3;
            case 4:
                return yCoordinatesSample4;
            case 5:
                return yCoordinatesSample5;
            default:
                throw new Exception("Wrong Y coordinate number");
        }
    }

    public static double[] GetTimePoints(int numberOfSignatureSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                return timePointsSample1;
            case 2:
                return timePointsSample2;
            case 3:
                return timePointsSample3;
            case 4:
                return timePointsSample4;
            case 5:
                return timePointsSample5;
            default:
                throw new Exception("Wrong time points number");
        }
    }

    public static double[] GetTouchings(int numberOfSignatureSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                return touchingSample1;
            case 2:
                return touchingSample2;
            case 3:
                return touchingSample3;
            case 4:
                return touchingSample4;
            case 5:
                return touchingSample5;
            default:
                throw new Exception("Wrong touching number");
        }
    }


    public static void SetNumberOfSignatureSamples(int numberOfSignatureSamplesToSet) {
        numberOfSignatureSamples = numberOfSignatureSamplesToSet;
    }

    public static void SetMaxSignatureLength(int newMaxSignatureLength) {
        maxSignatureLength = newMaxSignatureLength;
    }

    private static void SetSignatureNumberOfPoints(int numberOfSignaturePoints) {
        signatureNumberOfPointsList.add(numberOfSignaturePoints);
    }

    private void SetXCoordinates(int numberOfSignatureSample, double[] xCoordinatesSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                xCoordinatesSample1 = xCoordinatesSample;
                break;
            case 2:
                xCoordinatesSample2 = xCoordinatesSample;
                break;
            case 3:
                xCoordinatesSample3 = xCoordinatesSample;
                break;
            case 4:
                xCoordinatesSample4 = xCoordinatesSample;
                break;
            case 5:
                xCoordinatesSample5 = xCoordinatesSample;
                break;
            default:
                throw new Exception("Wrong X coordinate number");
        }
    }

    private void SetYCoordinates(int numberOfSignatureSample, double[] yCoordinatesSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                yCoordinatesSample1 = yCoordinatesSample;
                break;
            case 2:
                yCoordinatesSample2 = yCoordinatesSample;
                break;
            case 3:
                yCoordinatesSample3 = yCoordinatesSample;
                break;
            case 4:
                yCoordinatesSample4 = yCoordinatesSample;
                break;
            case 5:
                yCoordinatesSample5 = yCoordinatesSample;
                break;
            default:
                throw new Exception("Wrong Y coordinate number");
        }
    }

    private void SetTimePoints(int numberOfSignatureSample, double[] timePointsSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                timePointsSample1 = timePointsSample;
                break;
            case 2:
                timePointsSample2 = timePointsSample;
                break;
            case 3:
                timePointsSample3 = timePointsSample;
                break;
            case 4:
                timePointsSample4 = timePointsSample;
                break;
            case 5:
                timePointsSample5 = timePointsSample;
                break;
            default:
                throw new Exception("Wrong time point number");
        }
    }

    private void SetTouchings(int numberOfSignatureSample, double[] touchingSample) throws Exception {

        switch (numberOfSignatureSample) {
            case 1:
                touchingSample1 = touchingSample;
                break;
            case 2:
                touchingSample2 = touchingSample;
                break;
            case 3:
                touchingSample3 = touchingSample;
                break;
            case 4:
                touchingSample4 = touchingSample;
                break;
            case 5:
                touchingSample5 = touchingSample;
                break;
            default:
                throw new Exception("Wrong touching sample number");
        }
    }
    //endregion
}