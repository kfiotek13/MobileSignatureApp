package com.studio.hsx77.signatureverifier.SignatureHelpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;


public class SignatureDrawer extends View {

    private Paint paint = new Paint();
    private Path path = new Path();
    Context context;

    float[] X, Y;
    int[] touchings;

    Activity activity;
    Canvas canvas = new Canvas();

    public SignatureDrawer(Context context) {
        super(context);
        this.context = context;
    }

    public SignatureDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public SignatureDrawer(Activity activity, Context context, float[] X, float[] Y, int[] touchings) {
        super(context);

        this.activity = activity;
        this.context = context;

        this.X = X;
        this.Y = Y;
        this.touchings = touchings;

        SetPaint();
        draw(canvas);
    }


    private void SetPaint() {
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(5f);

    }


    @Override
    protected void onDraw(Canvas canvas) {

        float scalingFactor = SetScalingFactor(X, Y);

        path.moveTo(X[0]*scalingFactor, Y[0]*scalingFactor);

        for (int i = 0; i < X.length; i++) {
            if (touchings[i] == 0) {
                path.moveTo(X[i+1]*scalingFactor, Y[i+1]*scalingFactor);
            }
            path.lineTo(X[i] * scalingFactor, Y[i] * scalingFactor);
        }
        canvas.drawPath(path, paint);
        path.reset();
    }


    private float SetScalingFactor(float[] X, float[] Y) {

        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        float maxScreenX = dm.widthPixels;
        float maxScreenY = dm.heightPixels;

        float maxX = GetMax(X);
        float maxY = GetMax(Y);

        float maxXScalingFactor = maxScreenX / maxX * 0.8f;
        float maxYScalingFactor = maxScreenY / maxY * 0.8f;

        if (maxScreenY > maxXScalingFactor * maxY)
            return maxXScalingFactor;
        else {
            if (maxScreenX > maxYScalingFactor * maxX)
                return maxYScalingFactor;
            else
                return 1f;
        }
    }

    private static float GetMax(float[] inputArray) {
        float maxValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] > maxValue) {
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }

}
