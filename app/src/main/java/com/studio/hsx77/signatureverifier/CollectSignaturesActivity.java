package com.studio.hsx77.signatureverifier;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.studio.hsx77.signatureverifier.SignatureHelpers.SignatureCollector;
import com.studio.hsx77.signatureverifier.SignatureHelpers.SignatureCreator;
import com.studio.hsx77.signatureverifier.SignatureHelpers.SignatureModel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class CollectSignaturesActivity extends AppCompatActivity {

    //region Private Fields

    static final int numberOfSignatureSamples = 5;
    Button btnClear, btnAddNSave, btnCancel;
    LinearLayout lnContent;
    View view;
    SignatureCreator signatureCreator;
    SignatureModel signatureModel;
    TextView txtWrittenSignature;

    static String userFileName;
    private static final int SAVE_SIGNATURE_PERMISSION_REQUEST_CODE = 1;

//endregion

    //region Override Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_signatures);

        btnClear = findViewById(R.id.btnClear);
        btnAddNSave = findViewById(R.id.btnAddNSave);
        btnCancel = findViewById(R.id.btnCancel);

        btnAddNSave.setEnabled(false);

        btnAddNSave.setOnClickListener(onButtonClick);
        btnClear.setOnClickListener(onButtonClick);
        btnCancel.setOnClickListener(onButtonClick);

        lnContent = findViewById(R.id.lnLayoutSignature);
        signatureCreator = new SignatureCreator(getApplicationContext(), null, btnAddNSave);
        signatureCreator.setBackgroundColor(Color.WHITE);

        lnContent.addView(signatureCreator, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        view = lnContent;

        txtWrittenSignature = findViewById(R.id.txtWrittenSignatures);
        txtWrittenSignature.setText(getString(R.string.txt_collected_signatures, SignatureCollector.GetNumberOfSignatureSamples()));

        if (userFileName == null)
            GetNameForSavedSignature();
    }

    @SuppressWarnings("unchecked")
    Button.OnClickListener onButtonClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnClear) {
                signatureCreator.ClearView();
                btnAddNSave.setEnabled(false);
            } else if (v == btnAddNSave) {
                if (SignatureCollector.GetNumberOfSignatureSamples() < numberOfSignatureSamples) {
                    AddNewSignature();

                    if (SignatureCollector.GetNumberOfSignatureSamples() == numberOfSignatureSamples) {
                        view.setFocusable(false);
                        view.setClickable(false);
                        btnAddNSave.setText(R.string.btn_save);
                        btnAddNSave.setBackgroundColor(0xFF00FF00);
                        signatureModel = new SignatureModel();
                        if (signatureModel.GetInformationIfModelCreationWasCorrect()) {
                            btnAddNSave.setEnabled(true);
                            btnClear.setEnabled(false);
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.txt_model_failed, Toast.LENGTH_LONG).show();
                            FinishActivityAndResetCollectedSignatureData();
                        }

                    } else {
                        signatureCreator.ClearView();
                    }
                } else {
                    if (SaveIfStoragePermissionIsGranted()) {
                        SaveSignature();
                    }
                    FinishActivityAndResetCollectedSignatureData();
                }
            } else if (v == btnCancel) {
                SetAlertDialogToDecideWhetherSurelyCancel();
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case SAVE_SIGNATURE_PERMISSION_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    SaveSignature();
        }
    }

    @Override
    public void onBackPressed() {
        ResetCollectedSignatureData();
        super.onBackPressed();
    }

//endregion

// region Private Methods

    private void AddNewSignature() {
        try {
            new SignatureCollector(signatureCreator.GetXCoordinatesList(), signatureCreator.GetYCoordinatesList(), signatureCreator.GetTimePointsList(), signatureCreator.GetTouchingsList());
            txtWrittenSignature.setText(getString(R.string.txt_collected_signatures, SignatureCollector.GetNumberOfSignatureSamples()));
            signatureCreator.ClearSignatureFeaturesLists();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CheckIfExistsFileByTheName() {
        File directoryPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SignatureVerifier/");
        if (directoryPath.exists()) {
            File file = new File(directoryPath, userFileName + ".txt");
            if (file.exists()) {
                SetAlertDialogToDecideWhatMakesWithTheSameFileName();
            }
        }
    }

    private void FinishActivityAndResetCollectedSignatureData() {
        Intent intent = new Intent(CollectSignaturesActivity.this, MainVerifierActivity.class);
        startActivity(intent);
        ResetCollectedSignatureData();
        CollectSignaturesActivity.this.finish();
    }

    private void GetNameForSavedSignature() {
        final EditText txtFileName = new EditText(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.txt_write_name)
                .setView(txtFileName)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        userFileName = txtFileName.getText().toString();
                        CheckIfExistsFileByTheName();
                    }
                });
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intent = new Intent(CollectSignaturesActivity.this, MainVerifierActivity.class);
                startActivity(intent);
            }
        });
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

        txtFileName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        });
    }

    private boolean HasReadToExternalStoragePermissions() {
        return (getApplicationContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean HasWriteToExternalStoragePermissions() {
        return (getApplicationContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean IsExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private void ResetCollectedSignatureData() {
        userFileName = null;
        SignatureCollector.SetNumberOfSignatureSamples(0);
        SignatureCollector.SetMaxSignatureLength(0);
        txtWrittenSignature.setText(getString(R.string.txt_collected_signatures, SignatureCollector.GetNumberOfSignatureSamples()));
    }

    private boolean SaveIfStoragePermissionIsGranted() {

        if (HasReadToExternalStoragePermissions() && HasWriteToExternalStoragePermissions()) {
            return true;
        }

        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, SAVE_SIGNATURE_PERMISSION_REQUEST_CODE);
        return false;
    }

    @SuppressWarnings("all")
    private void SaveSignature() {
        if (!IsExternalStorageWritable()) {
            Toast.makeText(this, R.string.txt_notWritable, Toast.LENGTH_LONG).show();
        }

        try {
            File directoryPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SignatureVerifier/");
            if (!directoryPath.exists()) {
                directoryPath.mkdir();
            }

            File newFile = new File(directoryPath, userFileName + ".txt");
            BufferedWriter writer = new BufferedWriter(new FileWriter(newFile));

            double[] modelSignatureX = signatureModel.GetModelSignatureX();
            double[] modelSignatureY = signatureModel.GetModelSignatureY();
            double[] modelSignatureSpeed = signatureModel.GetModelSpeedOfSignature();
            int[] modelSignatureTouch = signatureModel.GetModelSignatureTouchings();

            writer.write(Integer.toString(modelSignatureX.length));
            writer.write("\r\n");
            writer.write(Double.toString(signatureModel.GetMinTimeOfSignatureCreation()));
            writer.write("\r\n");
            writer.write(Double.toString(signatureModel.GetMaxTimeOfSignatureCreation()));
            writer.write("\r\n");
            for (int i = 0; i < modelSignatureX.length; i++) {
                writer.write(Double.toString(modelSignatureX[i]) + " ");
                writer.write(Double.toString(modelSignatureY[i]) + " ");
                writer.write(Double.toString(modelSignatureSpeed[i]) + " ");
                writer.write(Integer.toString(modelSignatureTouch[i]));
                writer.write("\r\n");
            }
            writer.flush();
            writer.close();
            Toast.makeText(getApplicationContext(), R.string.txt_model_saved, Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.txt_saving_exception, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.txt_saving_exception, Toast.LENGTH_SHORT).show();
        }
    }

    private void SetAlertDialogToDecideWhetherSurelyCancel() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.txt_really_cancel)
                .setPositiveButton(R.string.txt_yes_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        FinishActivityAndResetCollectedSignatureData();
                    }
                });
        builder.setNegativeButton(R.string.txt_no_return, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void SetAlertDialogToDecideWhatMakesWithTheSameFileName() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.txt_file_same_name)
                .setPositiveButton(R.string.txt_create_new_name, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        userFileName = "";
                        GetNameForSavedSignature();
                    }
                });
        builder.setNegativeButton(R.string.txt_delete_content, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                try {
                    new PrintWriter(Environment.getExternalStorageDirectory() + "/SignatureVerifier/" + userFileName + ".txt").close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    //endregion
}
