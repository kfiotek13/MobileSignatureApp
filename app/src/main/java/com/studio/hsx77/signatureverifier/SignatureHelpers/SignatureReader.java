package com.studio.hsx77.signatureverifier.SignatureHelpers;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import com.studio.hsx77.signatureverifier.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Double.parseDouble;
import static java.lang.Float.parseFloat;

public class SignatureReader {

    //region Private Fields

    private double minTotalSpeed, maxTotalSpeed;

    private double[] X_toVerify, Y_toVerify, TimePoints_toVerify;
    private float[] X_toPresent, Y_toPresent;
    private int[] touch_ToPresent;

    private Context context;

    //endregion

    public SignatureReader(Context context, String fileName, boolean isForVerification) {

        this.context = context;
        ReadSignature(fileName, isForVerification);
    }


    private void ReadSignature(String fileName, boolean isForVerification) {

        if (!IsExternalStorageReadable()) {
            Toast.makeText(context, R.string.txt_notReadable, Toast.LENGTH_LONG).show();
        } else {
            File sdCard = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SignatureVerifier/");
            if (!sdCard.exists()) {
                Toast.makeText(context, R.string.txt_reading_error, Toast.LENGTH_LONG).show();
            } else {
                File file = new File(sdCard, fileName + ".txt");
                if (!file.exists()) {
                    Toast.makeText(context, R.string.txt_reading_error_file, Toast.LENGTH_LONG).show();
                } else {
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(file));

                        String currentLine;

                        int numberOfSignaturePoints = Integer.parseInt(br.readLine());
                        InitializeArrays(isForVerification, numberOfSignaturePoints);

                        minTotalSpeed = Double.parseDouble(br.readLine());
                        maxTotalSpeed = Double.parseDouble(br.readLine());

                        int i = 0;
                        while ((currentLine = br.readLine()) != null) {
                            String[] stringParts = currentLine.split(" ");

                            if (isForVerification) {
                                X_toVerify[i] = parseDouble(stringParts[0]);
                                Y_toVerify[i] = parseDouble(stringParts[1]);
                                TimePoints_toVerify[i] = parseDouble(stringParts[2]);
                            } else {
                                X_toPresent[i] = parseFloat(stringParts[0]);
                                Y_toPresent[i] = parseFloat(stringParts[1]);
                                touch_ToPresent[i] = Integer.parseInt(stringParts[3]);
                            }
                            i++;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(context, R.string.txt_reading_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public static String[] FileNamesTable(Context context) {
        ArrayList<String> listToConvert = CreateListWithFileNames(context);

        if (listToConvert != null && !listToConvert.isEmpty()) {
            String[] fileNameTable = new String[listToConvert.size()];
            return listToConvert.toArray(fileNameTable);
        }
        return new String[0];
    }

    private static ArrayList<String> CreateListWithFileNames(Context context) {

        ArrayList<String> fileNameList = new ArrayList<>();

        if (!IsExternalStorageReadable()) {
            Toast.makeText(context, R.string.txt_notReadable, Toast.LENGTH_LONG).show();
        } else {
            File sdCard = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SignatureVerifier/");

            if (!sdCard.exists()) {
                Toast.makeText(context, R.string.txt_reading_error, Toast.LENGTH_LONG).show();
            } else {
                File[] files = sdCard.listFiles();

                for (File file : files) {
                    String filePath = file.getPath();
                    if (filePath.endsWith(".txt")) {
                        String fileName = filePath.substring(filePath.lastIndexOf("/") + 1, filePath.indexOf(".txt"));
                        fileNameList.add(fileName);
                    }
                }
                return fileNameList;
            }
            return null;
        }
        return null;
    }

    public double GetMinTotalSpeed() {
        return minTotalSpeed;
    }

    public double GetMaxTotalSpeed() {
        return maxTotalSpeed;
    }

    public double[] GetX() {
        return X_toVerify;
    }

    public double[] GetY() {
        return Y_toVerify;
    }

    public double[] GetTimePoints() {
        return TimePoints_toVerify;
    }

    public float[] GetX_ToPresent() {
        return X_toPresent;
    }

    public float[] GetY_ToPresent() {
        return Y_toPresent;
    }

    public int[] GetTouch_ToPresent() {
        return touch_ToPresent;
    }

    private void InitializeArrays(boolean isForVerification, int arraySize) {
        if (isForVerification) {
            X_toVerify = new double[arraySize];
            Y_toVerify = new double[arraySize];
            TimePoints_toVerify = new double[arraySize];
        } else {
            X_toPresent = new float[arraySize];
            Y_toPresent = new float[arraySize];
            touch_ToPresent = new int[arraySize];
        }
    }

    @SuppressWarnings("all")
    private static boolean IsExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }
}
