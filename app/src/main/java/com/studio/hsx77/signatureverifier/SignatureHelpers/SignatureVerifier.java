package com.studio.hsx77.signatureverifier.SignatureHelpers;

import android.annotation.SuppressLint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SignatureVerifier {

    private static final double error = 0.1;

    @SuppressLint("UseSparseArrays")
    private static Map<Integer, List<Double>> modelSpeedHistogram = new HashMap<>();
    @SuppressLint("UseSparseArrays")
    private static Map<Integer, List<Double>> toVerifySpeedHistogram = new HashMap<>();
    private static int numberOfAttempts = 3;

    public SignatureVerifier() {
        numberOfAttempts--;
    }

    //region Private Methods

    private ArrayList<Double> CalculateLengthsBetweenPoints(double[] X, double[] Y) {
        ArrayList<Double> lengthsList = new ArrayList<>();

        for (int i = 0; i < X.length - 1; i++) {
            lengthsList.add(Math.hypot(X[i + 1] - X[i], Y[i + 1] - Y[i]));
        }
        return lengthsList;
    }

    private double CalculateSumOfLengths(ArrayList<Double> lengthsListToSum) {
        double sum = 0;
        for (Double d : lengthsListToSum)
            sum += d;
        return sum;
    }

    private double CheckSimilarityOfSignaturesByTotalLengthUsingManhattanDistance(double[] modelX, double[] modelY, double[] toCheckX, double[] toCheckY) {

        ArrayList<Double> modelSignatureLengths = CalculateLengthsBetweenPoints(modelX, modelY);
        ArrayList<Double> resizedLengthsToCheck = ResizeLengths(modelSignatureLengths, CalculateLengthsBetweenPoints(toCheckX, toCheckY));

        double modelTotalLength = CalculateSumOfLengths(modelSignatureLengths);
        double toCheckTotalLength = CalculateSumOfLengths(resizedLengthsToCheck);

        return Math.abs(modelTotalLength - toCheckTotalLength);
    }

    private double CheckSimilarityOfSignaturesByHistogramsOfSpeedUsingEuclideanDistance(double[] modelSpeed, double[] speedToVerify, double maxTimeOfSignatureCreation, double minTimeOfSignatureCreation) {

        double totalTimeOfSpeedToVerify = GetSumOfValues(speedToVerify);
        if (totalTimeOfSpeedToVerify < minTimeOfSignatureCreation - minTimeOfSignatureCreation * error
                || totalTimeOfSpeedToVerify > maxTimeOfSignatureCreation + maxTimeOfSignatureCreation * error) {
            return 1000.00; // Such a great number of means lack of similarity
        }

        CreateHistogramOfModelSpeed(modelSpeed, speedToVerify);

        List<Double> euclideanSqrtElementsList = new ArrayList<>();

        for (Map.Entry<Integer, List<Double>> modelSpeedHist : modelSpeedHistogram.entrySet()) {

            double model = modelSpeedHist.getValue().size();
            double verify = toVerifySpeedHistogram.get(modelSpeedHist.getKey()).size();
            euclideanSqrtElementsList.add(Math.pow(model - verify, 2));
        }

        double euclideanDistance = 0.0d;

        for (Double anEuclideanSqrtElementsList : euclideanSqrtElementsList) {
            euclideanDistance += anEuclideanSqrtElementsList;
        }
        return Math.sqrt(euclideanDistance);
    }

    private void CreateHistogramOfModelSpeed(double[] modelSpeed, double[] speedToVerify) {
        double divisor = 15.0;

        double maxModelSpeed = GetMax(modelSpeed);
        double minModelSpeed = GetMin(modelSpeed);

        double differenceBetweenExtremeSizes = maxModelSpeed - minModelSpeed;
        double section = differenceBetweenExtremeSizes / divisor;

        double temp = -1.0;
        List<Double> histValues;
        List<Double> histValues2;

        for (int i = 1; i <= divisor; i++) {

            histValues = new ArrayList<>();
            histValues2 = new ArrayList<>();

            double newTemp = temp + section;

            for (double a : modelSpeed) {
                if (a > temp && a <= newTemp) {
                    histValues.add(a);
                }
            }

            for (double a : speedToVerify) {
                if (a > temp && a <= newTemp) {
                    histValues2.add(a);
                }
            }
            modelSpeedHistogram.put(i, histValues);
            toVerifySpeedHistogram.put(i, histValues2);

            temp = newTemp;
        }
    }

    private double GetMax(double[] inputArray) {
        double maxValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] > maxValue) {
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }

    private double GetMin(double[] inputArray) {
        double minValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] < minValue) {
                minValue = inputArray[i];
            }
        }
        return minValue;
    }

    private double GetSumOfValues(double[] tableToSumUp) {
        double sum = 0.0d;
        for (double valueToSum : tableToSumUp) {
            sum += valueToSum;
        }
        return sum;
    }

    private ArrayList<Double> ResizeLengths(ArrayList<Double> modelLengthsList, ArrayList<Double> lengthsToCheckList) {
        for (int i = 0; i < modelLengthsList.size(); i++) {

            double resizedLengthToCheck = 0.0;
            double lengthToCheck = lengthsToCheckList.get(i);

            if (lengthToCheck != 0.0)
                resizedLengthToCheck = lengthToCheck * (modelLengthsList.get(i) / lengthToCheck);

            lengthsToCheckList.set(i, resizedLengthToCheck);
        }
        return lengthsToCheckList;
    }

    //endregion

    //region Public Methods

    public static int GetNumberOfAttempts() {
        return numberOfAttempts;
    }

    public static void ResetNumberOfAttempts() {
        numberOfAttempts = 3;
    }

    public boolean VerifySignatureUsingManhattanAndEuclideanDistance(double[] modelX, double[] modelY,
                                                                     double[] toCheckX, double[] toCheckY, double[] modelSpeed, double[] speedToVerify,
                                                                     double maxTimeOfSignatureCreation, double minTimeOfSignatureCreation) {

        double similarityByTotalLength = CheckSimilarityOfSignaturesByTotalLengthUsingManhattanDistance(modelX, modelY, toCheckX, toCheckY);

        double similarityBySpeeds = CheckSimilarityOfSignaturesByHistogramsOfSpeedUsingEuclideanDistance(modelSpeed, speedToVerify, maxTimeOfSignatureCreation, minTimeOfSignatureCreation);

        if (similarityBySpeeds < 1000) {
            if (modelX.length < 120) {
                if (similarityByTotalLength <= 20.0 && similarityBySpeeds <= 10.0)
                    return true;
                else if (similarityByTotalLength <= 20.0 && similarityBySpeeds > 10.0) {
                    return similarityByTotalLength <= 10.0 && similarityBySpeeds <= 15.0;
                } else
                    return similarityByTotalLength > 20.0 && similarityBySpeeds <= 6.0 && similarityByTotalLength <= 40.0;
            }
            else if (modelX.length < 170) {
                if (similarityByTotalLength <= 30.0 && similarityBySpeeds <= 30.0)
                    return true;
                else if (similarityByTotalLength <= 30.0 && similarityBySpeeds > 30.0) {
                    return similarityByTotalLength <= 20.0 && similarityBySpeeds <= 37.0;
                } else
                    return similarityByTotalLength > 30.0 && similarityBySpeeds <= 27.0 && similarityByTotalLength <= 60.0;
            } else if (modelX.length < 260) {
                if (similarityByTotalLength <= 50.0 && similarityBySpeeds <= 40.0)
                    return true;
                else if (similarityByTotalLength <= 50.0 && similarityBySpeeds > 40.0) {
                    return similarityByTotalLength <= 70.0 && similarityBySpeeds <= 60.0;
                } else
                    return similarityByTotalLength > 50.0 && similarityBySpeeds <= 30.0 && similarityByTotalLength <= 80.0;
            } else if (modelX.length < 400) {
                if (similarityByTotalLength <= 70.0 && similarityBySpeeds <= 70.0)
                    return true;
                else if (similarityByTotalLength <= 70.0 && similarityBySpeeds > 70.0) {
                    return similarityByTotalLength <= 60.0 && similarityBySpeeds <= 80.0;
                } else
                    return similarityByTotalLength > 70.0 && similarityBySpeeds <= 60.0 && similarityByTotalLength <= 110.0;
            } else {
                if (similarityByTotalLength <= 80.0 && similarityBySpeeds <= 90.0)
                    return true;
                else if (similarityByTotalLength <= 80.0 && similarityBySpeeds > 90.0) {
                    return similarityByTotalLength <= 70.0 && similarityBySpeeds <= 100.0;
                } else
                    return similarityByTotalLength > 80.0 && similarityBySpeeds <= 80.0 && similarityByTotalLength <= 140.0;
            }
        } else
            return false;
    }
    //endregion

}
