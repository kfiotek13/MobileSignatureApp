package com.studio.hsx77.signatureverifier.SignatureHelpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class SignatureCreator extends View {

    private static final float STROKE_WIDTH = 5f;
    private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;

    private float lastTouchX;
    private float lastTouchY;

    Button btnAdd;

    ArrayList<Float> xCoordinatesList, yCoordinatesList, timePointList;
    ArrayList<Double> touchingsList;

    private final RectF dirtyRect = new RectF();

    private Paint paint = new Paint();
    private Path path = new Path();

    public SignatureCreator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(STROKE_WIDTH);
        yCoordinatesList = new ArrayList<>();
        xCoordinatesList = new ArrayList<>();
        timePointList = new ArrayList<>();
        touchingsList = new ArrayList<>();
    }

    public SignatureCreator(Context context, AttributeSet attributeSet, Button buttonToChangeState) {
        super(context, attributeSet);
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(STROKE_WIDTH);
        yCoordinatesList = new ArrayList<>();
        xCoordinatesList = new ArrayList<>();
        timePointList = new ArrayList<>();
        touchingsList = new ArrayList<>();

        btnAdd = buttonToChangeState;
    }

    public void ClearView() {
        path.reset();
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(path, paint);
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();

        if (btnAdd != null)
            btnAdd.setEnabled(true);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                path.moveTo(eventX, eventY);
                lastTouchX = eventX;
                lastTouchY = eventY;

                if (!touchingsList.isEmpty())
                    touchingsList.remove(touchingsList.size() - 1);

                touchingsList.add(0.0d);
                return true;

            case MotionEvent.ACTION_MOVE:

            case MotionEvent.ACTION_UP:

                ResetDirtyRect(eventX, eventY);
                int historySize = event.getHistorySize();

                for (int i = 0; i < historySize; i++) {
                    float historicalX = event.getHistoricalX(i);
                    float historicalY = event.getHistoricalY(i);
                    float historicalTime = event.getHistoricalEventTime(i);

                    CollectCoordinatesAndTimeData(historicalX, historicalY, historicalTime);

                    ExpandDirtyRect(historicalX, historicalY);
                    path.lineTo(historicalX, historicalY);
                }
                path.lineTo(eventX, eventY);
                break;

            default:
                return false;
        }

        invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

        lastTouchX = eventX;
        lastTouchY = eventY;

        return true;
    }

    @SuppressWarnings("unchecked")
    private void CollectCoordinatesAndTimeData(float X, float Y, float time) {
        xCoordinatesList.add(X);
        yCoordinatesList.add(Y);
        timePointList.add(time);

        touchingsList.add(1.0d);
    }

    private void ExpandDirtyRect(float historicalX, float historicalY) {
        if (historicalX < dirtyRect.left) {
            dirtyRect.left = historicalX;
        } else if (historicalX > dirtyRect.right) {
            dirtyRect.right = historicalX;
        }

        if (historicalY < dirtyRect.top) {
            dirtyRect.top = historicalY;
        } else if (historicalY > dirtyRect.bottom) {
            dirtyRect.bottom = historicalY;
        }
    }

    private void ResetDirtyRect(float eventX, float eventY) {
        dirtyRect.left = Math.min(lastTouchX, eventX);
        dirtyRect.right = Math.max(lastTouchX, eventX);
        dirtyRect.top = Math.min(lastTouchY, eventY);
        dirtyRect.bottom = Math.max(lastTouchY, eventY);
    }

    public void ClearSignatureFeaturesLists() {
        xCoordinatesList.clear();
        yCoordinatesList.clear();
        timePointList.clear();
        touchingsList.clear();
    }

    //region Public Properties

    public ArrayList<Float> GetXCoordinatesList() {
        if (btnAdd != null)
            btnAdd.setEnabled(false);

        return xCoordinatesList;
    }

    public ArrayList<Float> GetYCoordinatesList() {
        return yCoordinatesList;
    }

    public ArrayList<Float> GetTimePointsList() {
        return timePointList;
    }

    public ArrayList<Double> GetTouchingsList() {

        if (touchingsList.size() > xCoordinatesList.size())
            touchingsList.remove(touchingsList.size() - 1);

        return touchingsList;
    }

    //endregion
}
