package com.studio.hsx77.signatureverifier;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainVerifierActivity extends AppCompatActivity {

    Button btnAddSignature, btnVerifySignature, btnSeeSignature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_verifier);

        btnAddSignature = findViewById(R.id.btnAddSignature);
        btnVerifySignature = findViewById(R.id.btnVerifySignature);
        btnSeeSignature = findViewById(R.id.btnSeeSignature);

        btnAddSignature.setOnClickListener(onButtonClick);
        btnVerifySignature.setOnClickListener(onButtonClick);
        btnSeeSignature.setOnClickListener(onButtonClick);
    }

    Button.OnClickListener onButtonClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnAddSignature) {
                Intent i = new Intent(MainVerifierActivity.this, CollectSignaturesActivity.class);
                startActivity(i);
            } else if (v == btnVerifySignature) {
                Intent i = new Intent(MainVerifierActivity.this, VerifySignatureActivity.class);
                startActivity(i);
            }
            else if (v == btnSeeSignature) {
                Intent i = new Intent(MainVerifierActivity.this, PresentSignatureActivity.class);
                startActivity(i);
            }
        }
    };
}
