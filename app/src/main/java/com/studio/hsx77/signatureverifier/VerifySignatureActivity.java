package com.studio.hsx77.signatureverifier;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.studio.hsx77.signatureverifier.SignatureHelpers.SignalModelling;
import com.studio.hsx77.signatureverifier.SignatureHelpers.SignatureCreator;
import com.studio.hsx77.signatureverifier.SignatureHelpers.SignatureReader;
import com.studio.hsx77.signatureverifier.SignatureHelpers.SignatureVerifier;

import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import java.util.ArrayList;

public class VerifySignatureActivity extends AppCompatActivity {

    Button btnCancel, btnVerify, btnClear;
    static String fileName;
    SignatureReader modelSignature;
    SignatureVerifier signatureVerifier;
    LinearLayout lnContent;
    View view;
    SignatureCreator signatureCreator;
    TextView txtAuthenticatedUser;
    private double[] X_toVerify, Y_toVerify, TimePoints_toVerify,
            X_preparedToVerify, Y_preparedToVerify, TimePoints_preparedToVerify;
    private int maxModelSignatureLength;
    private boolean isFirstViewLaunching = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_signature);

        btnCancel = findViewById(R.id.btnCancel);
        btnClear = findViewById(R.id.btnClear);
        btnVerify = findViewById(R.id.btnVerify);
        btnVerify.setEnabled(false);

        txtAuthenticatedUser = findViewById(R.id.txtAuthenticatedUser);

        if (fileName == null)
            ShowListWithFileNames();
        else {
            txtAuthenticatedUser.setText(getString(R.string.txt_authenticated_user, fileName));
            modelSignature = new SignatureReader(getApplicationContext(), fileName, true);
        }

        btnCancel.setOnClickListener(onButtonClick);
        btnVerify.setOnClickListener(onButtonClick);
        btnClear.setOnClickListener(onButtonClick);

        SetView();
        isFirstViewLaunching = false;
    }

    Button.OnClickListener onButtonClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnClear) {
                signatureCreator.ClearView();
            } else if (v == btnVerify) {
                signatureVerifier = new SignatureVerifier();
                PrepareSignatureToVerify();
                boolean resultOfVerification = signatureVerifier.VerifySignatureUsingManhattanAndEuclideanDistance
                        (modelSignature.GetX(), modelSignature.GetY(), X_preparedToVerify, Y_preparedToVerify,
                                modelSignature.GetTimePoints(), TimePoints_preparedToVerify,
                                modelSignature.GetMaxTotalSpeed(), modelSignature.GetMinTotalSpeed());
                if (resultOfVerification)
                    DeclareCorrectVerification();
                else
                    DeclareInvalidVerification(SignatureVerifier.GetNumberOfAttempts());
            } else if (v == btnCancel) {
                SetAlertDialogToDecideWhetherSurelyCancel();
            }
        }
    };

    private void DeclareCorrectVerification() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(getString(R.string.txt_correct_verification, fileName))
                .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        SignatureVerifier.ResetNumberOfAttempts();
                        fileName = null;
                        Intent intent = new Intent(VerifySignatureActivity.this, MainVerifierActivity.class);
                        startActivity(intent);
                        VerifySignatureActivity.this.finish();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void DeclareInvalidVerification(int numberOfAttempts) {
        if (numberOfAttempts > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.txt_wrong_verification_attempt, numberOfAttempts))
                    .setPositiveButton(R.string.btn_keep_trying, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            SetView();
                        }
                    });
            builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    fileName = null;
                    SignatureVerifier.ResetNumberOfAttempts();
                    Intent intent = new Intent(VerifySignatureActivity.this, MainVerifierActivity.class);
                    startActivity(intent);
                    VerifySignatureActivity.this.finish();
                }
            });
            builder.setCancelable(false);
            final AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.txt_wrong_verification))
                    .setNegativeButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            SignatureVerifier.ResetNumberOfAttempts();
                            fileName = null;
                            Intent intent = new Intent(VerifySignatureActivity.this, MainVerifierActivity.class);
                            startActivity(intent);
                            VerifySignatureActivity.this.finish();
                        }
                    });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private double[] FloatToDoubleConverter(ArrayList<Float> listToConvert) {
        double[] convertedTable = new double[listToConvert.size()];

        for (int i = 0; i < listToConvert.size(); i++) {
            convertedTable[i] = (double) listToConvert.get(i);
        }
        return convertedTable;
    }

    @Override
    public void onBackPressed() {
        fileName = null;
        SignatureVerifier.ResetNumberOfAttempts();
        super.onBackPressed();
    }

    private void PrepareSignatureToVerify() {

        SetMaxModelSignatureLength();
        SetDataToVerify();

        PolynomialSplineFunction xSignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(X_toVerify, 1000));
        PolynomialSplineFunction ySignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(Y_toVerify, 1000));
        PolynomialSplineFunction speedSignalToResample = SignalModelling.InterpolateData(SignalModelling.NormalizeData(TimePoints_toVerify, 1000));

        X_preparedToVerify = SignalModelling.ResampleData(maxModelSignatureLength, X_toVerify.length, xSignalToResample);
        Y_preparedToVerify = SignalModelling.ResampleData(maxModelSignatureLength, Y_toVerify.length, ySignalToResample);
        TimePoints_preparedToVerify = SignalModelling.ResampleData(maxModelSignatureLength, TimePoints_toVerify.length, speedSignalToResample);
    }

    private void SetDataToVerify() {
        X_toVerify = FloatToDoubleConverter(signatureCreator.GetXCoordinatesList());
        Y_toVerify = FloatToDoubleConverter(signatureCreator.GetXCoordinatesList());
        TimePoints_toVerify = FloatToDoubleConverter(signatureCreator.GetTimePointsList());
    }

    private void SetMaxModelSignatureLength() {
        double[] modelX = modelSignature.GetX();
        maxModelSignatureLength = modelX.length;
    }

    private void SetView() {

        signatureCreator = new SignatureCreator(getApplicationContext(), null,btnVerify);
        signatureCreator.setBackgroundColor(Color.WHITE);

        if (isFirstViewLaunching) {
            lnContent = findViewById(R.id.lnLayoutMiddle);
            lnContent.addView(signatureCreator, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            view = lnContent;
        } else {
            ViewGroup parent = (ViewGroup) view.getParent();
            int index = parent.indexOfChild(view);
            int viewHeight = view.getHeight();
            parent.removeView(view);
            view = getLayoutInflater().inflate(R.layout.activity_verify_signature, parent, false);
            parent.addView(signatureCreator, index, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, viewHeight));
            view = signatureCreator;
        }
    }

    private void ShowListWithFileNames() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.txt_presented_signature_authentication);

        final String[] fileNameTable = SignatureReader.FileNamesTable(getApplicationContext());
        if (fileNameTable.length == 0) {
            builder.setMessage(R.string.txt_no_files_saved);
            builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(VerifySignatureActivity.this, MainVerifierActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            final int checkedItem = 0;
            fileName = fileNameTable[checkedItem];
            txtAuthenticatedUser.setText(getString(R.string.txt_authenticated_user, fileName));
            builder.setSingleChoiceItems(fileNameTable, checkedItem, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    fileName = fileNameTable[which];
                    txtAuthenticatedUser.setText(getString(R.string.txt_authenticated_user, fileName));
                }
            });

            builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    modelSignature = new SignatureReader(getApplicationContext(), fileName, true);
                }
            });
            builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    fileName = null;
                    Intent intent = new Intent(VerifySignatureActivity.this, MainVerifierActivity.class);
                    startActivity(intent);
                }
            });
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void SetAlertDialogToDecideWhetherSurelyCancel() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.txt_really_cancel)
                .setPositiveButton(R.string.txt_yes_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        fileName = null;
                        SignatureVerifier.ResetNumberOfAttempts();
                        Intent intent = new Intent(VerifySignatureActivity.this, MainVerifierActivity.class);
                        startActivity(intent);
                        VerifySignatureActivity.this.finish();
                    }
                });
        builder.setNegativeButton(R.string.txt_no_return, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }
}
