package com.studio.hsx77.signatureverifier;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.studio.hsx77.signatureverifier.SignatureHelpers.SignatureDrawer;
import com.studio.hsx77.signatureverifier.SignatureHelpers.SignatureReader;

public class PresentSignatureActivity extends AppCompatActivity {

    Button btnOther, btnReturn;
    SignatureReader signatureReader;
    SignatureDrawer signatureDrawer;
    static String fileName;
    int amountOfChangeView;
    TextView txtSignature;
    LinearLayout lnContent;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_present_signature);

        amountOfChangeView = 0;

        btnOther = findViewById(R.id.btnOther);
        btnReturn = findViewById(R.id.btnReturn);

        txtSignature = findViewById(R.id.txtUserName);

        if (fileName == null)
            ShowListWithFileNames();
        else
            SetView();

        btnOther.setOnClickListener(onButtonClick);
        btnReturn.setOnClickListener(onButtonClick);
    }

    Button.OnClickListener onButtonClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnOther) {
                amountOfChangeView++;
                ShowListWithFileNames();
            } else if (v == btnReturn) {
                fileName = null;
                Intent intent = new Intent(PresentSignatureActivity.this, MainVerifierActivity.class);
                startActivity(intent);
            }
        }
    };

    @Override
    public void onBackPressed() {
        fileName = null;
        super.onBackPressed();
    }

    private void SetView() {
        txtSignature.setText(getString(R.string.txt_signature, fileName));

        signatureReader = new SignatureReader(getApplicationContext(), fileName, false);
        signatureDrawer = new SignatureDrawer(this, getApplicationContext(), signatureReader.GetX_ToPresent(),
                signatureReader.GetY_ToPresent(), signatureReader.GetTouch_ToPresent());
        signatureDrawer.setBackgroundColor(Color.WHITE);

        if (amountOfChangeView == 0) {
            lnContent = findViewById(R.id.lnLayoutToPresent);
            lnContent.addView(signatureDrawer, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            view = lnContent;
        }
        if (amountOfChangeView > 0) {
            ViewGroup parent = (ViewGroup) view.getParent();
            int index = parent.indexOfChild(view);
            int viewHeight = view.getHeight();
            parent.removeView(view);
            view = getLayoutInflater().inflate(R.layout.activity_present_signature, parent, false);

            parent.addView(signatureDrawer, index, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, viewHeight));
            view = signatureDrawer;
        }
    }

    private void ShowListWithFileNames() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.txt_presented_signature);

        final String[] fileNameTable = SignatureReader.FileNamesTable(getApplicationContext());
        if (fileNameTable.length == 0) {
            builder.setMessage(R.string.txt_no_files_saved);
            builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(PresentSignatureActivity.this, MainVerifierActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            final int checkedItem = 0;
            fileName = fileNameTable[checkedItem];
            builder.setSingleChoiceItems(fileNameTable, checkedItem, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    fileName = fileNameTable[which];
                }
            });

            builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SetView();
                }
            });
            builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (amountOfChangeView == 0) {
                        Intent intent = new Intent(PresentSignatureActivity.this, MainVerifierActivity.class);
                        startActivity(intent);
                    } else {
                        amountOfChangeView--;
                        dialog.cancel();
                    }
                }
            });
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
}
