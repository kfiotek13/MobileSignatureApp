package com.studio.hsx77.signatureverifier.SignatureHelpers;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

public class SignalModelling {

    //region Public Methods

    public static double[] NormalizeData(double[] dataTable, double alpha) {
        double[] normalizedTable = new double[dataTable.length];
        double minOfList = GetMin(dataTable);
        double maxOfList = GetMax(dataTable);

        for (int n = 0; n < dataTable.length; n++) {
            normalizedTable[n] = (alpha * (dataTable[n] - minOfList) / (maxOfList - minOfList));
        }
        return normalizedTable;
    }

    public static PolynomialSplineFunction InterpolateData(double[] tableToInterpolate) {
        LinearInterpolator interpolator = new LinearInterpolator();
        return interpolator.interpolate(MakeAuxiliaryVector(tableToInterpolate.length), tableToInterpolate);
    }

    public static double[] ResampleData(int expectedTableLength, double currentSignatureLength, PolynomialSplineFunction estimateFunction) {

        double[] resampledData = new double[expectedTableLength];

        double divisor = Math.round((currentSignatureLength - 1.0) / expectedTableLength * 100000) / 100000.0d;
        double tempVariable = 0.0;

        for (int i = 0; i < expectedTableLength; i++) {
            resampledData[i] = estimateFunction.value(tempVariable);
            tempVariable += divisor;
        }
        return resampledData;
    }

    //endregion

    //region Private Methods

    private static double[] MakeAuxiliaryVector(int tableLength) {
        double[] table = new double[tableLength];

        for (int i = 0; i < tableLength; i++) {
            table[i] = i;
        }
        return table;
    }

    private static double GetMax(double[] inputArray) {
        double maxValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] > maxValue) {
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }

    private static double GetMin(double[] inputArray) {
        double minValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] < minValue) {
                minValue = inputArray[i];
            }
        }
        return minValue;
    }
    //endregion
}